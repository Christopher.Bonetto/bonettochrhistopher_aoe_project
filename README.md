**Intro**

This is a Unity project, replica of the Age of Empire game where I developed some of the main mechanics such
as the construction and positioning of buildings, the collection and control / management of resources and some upgrades for the units.
