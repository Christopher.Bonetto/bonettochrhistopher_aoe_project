﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ResourceScriptBehaviour : MonoBehaviour, IDamageable, ISelectionable
{
    [SerializeField] protected QuantityOfResources m_CurrentResourse;

    protected List<GameObject> m_WorkersAssignedToCollectList = new List<GameObject>();

    public int QuantityOfResource
    {
        get
        {
            return Mathf.Clamp(m_CurrentResourse.ResourceQuantity, 0, m_CurrentResourse.ResourceQuantity);
        }
        protected set
        {
            m_CurrentResourse.ResourceQuantity = value;
        }
    }

    public Materials TypeOfResource { get { return m_CurrentResourse.ResourceType; } }

    public GameObject m_ResourceDeposit;
    public GameObject ResourceDeposit
    {
        get
        {
            return m_ResourceDeposit;
        }
        set
        {
            m_ResourceDeposit = value;
        }
    }
    protected DepositBuildingActions m_DepositRef;
    protected float m_DistanceToDeposit;

    public IShowSelectionableInfo m_ResourceShowSelectionableInfo { get; protected set; }
    private int m_MaxResource = 0;

    [SerializeField] private Sprite m_ResourceSprite;



    public virtual void Awake()
    {
        m_ResourceShowSelectionableInfo = new ShowSelectedObject();
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        m_MaxResource = QuantityOfResource;
    }

    public virtual void Update()
    {
        CheckDeposit();
        
    }
    
    protected void CheckDeposit()
    {
        if (CivilizationScriptBehaviour.Instance != null)
        {
            if (CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList.Count == 1)
            {
                ResourceDeposit = CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList[0];
                m_DistanceToDeposit = Vector3.Distance(transform.position, ResourceDeposit.transform.position);
                CivilizationScriptBehaviour.Instance.DepositInstantiated = false;
            }
            else if(CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList.Count > 1 && CivilizationScriptBehaviour.Instance.DepositInstantiated)
            {
                CheckNearDeposit();
                if (CivilizationScriptBehaviour.Instance.Timer(5))
                {
                    CivilizationScriptBehaviour.Instance.DepositInstantiated = false;
                }
            }            
        }
    }


    protected void CheckNearDeposit()
    {
        foreach(GameObject Deposit in CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList)
        {
            m_DepositRef = Deposit.GetComponent<DepositBuildingActions>();

            for (int i = 0; i < m_DepositRef.ResourceCanBeAcquired.Length; i++)
            {                
                if (m_DepositRef.ResourceCanBeAcquired[i] == TypeOfResource)
                {
                    if (Vector3.Distance(transform.position, m_DepositRef.transform.position) < m_DistanceToDeposit)
                    {
                        ResourceDeposit = Deposit;
                    }
                }
            }
        }
    }

    public void AddWorkerToCollectResourceList(GameObject Worker)
    {
        if (!m_WorkersAssignedToCollectList.Contains(Worker) && Worker.GetComponent<WorkerActions>())
        {
            Worker.GetComponent<WorkerActions>().ResourceAssigned = this.gameObject;
            m_WorkersAssignedToCollectList.Add(Worker);
        }
    }

    public virtual bool TakeDamage(int Damage)
    {
        Damage = Mathf.Clamp(Damage, 0, QuantityOfResource);

        
        if (QuantityOfResource <= Damage)
        {
            QuantityOfResource -= Damage;
            
            Death();
            return true;
        }
        else
        {
            QuantityOfResource -= Damage;
            if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
            {
                ShowInfoPanels();
            }
            return false;
        }
    }
    
    public virtual void Death()
    {
        foreach (GameObject Worker in m_WorkersAssignedToCollectList)
        {
            if(Worker.GetComponent<UnitsScriptBehaviour>().FocusObject == this.gameObject)
            {
                Worker.GetComponent<NavMeshAgent>().SetDestination(Worker.transform.position);
                Worker.GetComponent<UnitsScriptBehaviour>().FocusObject = null;
            }
        }
        m_WorkersAssignedToCollectList.Clear();
        if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
        {
            MouseSelectionManager.Instance.ClearSelection();
        }
        Destroy(this.gameObject);
    }

    public virtual void ShowInfoPanels()
    {
        m_ResourceShowSelectionableInfo.ShowSelectionableInfo(gameObject.name, QuantityOfResource, m_MaxResource, 0, m_ResourceSprite, 0, 0, 0, 0, null);
    }
}
