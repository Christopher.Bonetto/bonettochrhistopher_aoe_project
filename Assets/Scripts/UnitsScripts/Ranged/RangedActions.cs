﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RangedActions : UnitsScriptBehaviour
{
    [SerializeField] private GameObject m_UnitBullet;
    [SerializeField] private GameObject m_SpawnSpoint;
    private ArrowActions m_BulletRef;

    public float BulletSpeed;

    
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void Attack()
    {
        ChangeUnitState(Actions.Attack);
        Shoot();
        m_CanAttack = false;
    }

    public void UpdateBulletValue(float AddThisValue)
    {
        BulletSpeed += AddThisValue;
    }

    protected virtual void Shoot()
    {
        GameObject arrow = Instantiate(m_UnitBullet, m_SpawnSpoint.transform.position, gameObject.transform.rotation) as GameObject;
        m_BulletRef = arrow.GetComponent<ArrowActions>();
        m_BulletRef.AddStartingForce(BulletSpeed);
        m_BulletRef.ArrowDamage = UnitStatisticsSO.Attack;
        m_BulletRef.ArrowFocussedObject = FocusObject;
    }
}
