﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;



public class UnitsScriptBehaviour : MonoBehaviour, IDamageable, ISelectionable
{
    
    public Actions m_CurrentUnitAction { get; protected set; }
    

    private UnitStatistics m_UnitStatisticsSO;
    public UnitStatistics UnitStatisticsSO
    {
        get
        {
            return m_UnitStatisticsSO;
        }
        set
        {
            m_UnitStatisticsSO = value;
        }
    }
    
    protected NavMeshAgent m_UnitAgent;
    public NavMeshAgent UnitAgent
    {
        get
        {
            return m_UnitAgent;
        }
        set
        {
            m_UnitAgent = value;
        }
    }

    private float m_Timer = 0f;
    protected bool m_CanAttack = true;

    protected IDamageable CanTakeDamage;

    public IShowSelectionableInfo m_UnitShowSelectionableInfo { get; protected set; }

    protected GameObject m_FocusObject = null;
    public GameObject FocusObject
    {
        get
        {
            return m_FocusObject;
        }
        set
        {
            m_FocusObject = value;
        }
    }

    protected int m_UnitCurrentHp = 10;



    public virtual void Awake()
    {
        m_UnitAgent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        m_UnitAgent.speed = m_UnitAgent.speed + UnitStatisticsSO.MovementSpeed;
        UpdateCurrentHp();

        m_UnitShowSelectionableInfo = new ShowSelectedObject();
    }
    

    public virtual void Update()
    {
        if (CheckFocussedObjectDistance() && m_CanAttack)
        {            
            Attack();
        }

        if (!m_CanAttack)
        {
            m_CanAttack = Timer(UnitStatisticsSO.TimeToAttack);
        }
        
        if(FocusObject != null)
        {
            gameObject.transform.LookAt(new Vector3(FocusObject.transform.position.x, gameObject.transform.position.y, FocusObject.transform.position.z));
        }

    }
    

    public virtual void ChangeUnitState(Actions NewAction)
    {
        m_CurrentUnitAction = NewAction;
    }

    
    public virtual void Attack()
    {
        ChangeUnitState(Actions.Attack);
        
        CanTakeDamage = FocusObject.GetComponent<IDamageable>() as IDamageable;
        if (CanTakeDamage != null)
        {
            CanTakeDamage.TakeDamage(UnitStatisticsSO.Attack);
            m_CanAttack = false;
        }
    }

    protected virtual bool CheckFocussedObjectDistance()
    {
        if (FocusObject != null)
        {
            
            if (Vector3.Distance(transform.position, FocusObject.transform.position) <= m_UnitAgent.stoppingDistance + FocusObject.transform.localScale.x + UnitStatisticsSO.ViewRadius)
            {
                m_UnitAgent.ResetPath();

                if (m_UnitAgent.velocity.sqrMagnitude == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if(m_UnitAgent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    ChangeUnitState(Actions.Move);
                    m_UnitAgent.SetDestination(FocusObject.transform.position);
                }
            }
        }
        else if(FocusObject == null)
        {
            
            if (m_UnitAgent.velocity.sqrMagnitude == 0 && !m_UnitAgent.pathPending && !m_UnitAgent.hasPath && m_CurrentUnitAction != Actions.Idle)
            {
                ChangeUnitState(Actions.Idle);
            }
            else
            {

            }
            return false;
        }
        return false;
    }

    public virtual void StopAgent()
    {
        m_UnitAgent.velocity = Vector3.zero;
    }

    public virtual bool Timer(float destinationTime)
    {
        m_Timer += Time.deltaTime;

        if (m_Timer >= destinationTime)
        {
            m_Timer = 0f;
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void UpdateCurrentHp()
    {
        m_UnitCurrentHp = UnitStatisticsSO.HealthMax;
    }
    

    public virtual bool TakeDamage(int Damage)
    {
        Damage = Mathf.Clamp(Damage, 0, UnitStatisticsSO.HealthMax + UnitStatisticsSO.Defence);
        
        if (m_UnitCurrentHp <= Damage)
        {
            m_UnitCurrentHp -= Damage;
            UIManager.Instance.DeactivateAllPanels();
            Death();
            return true;
        }
        else
        {
            m_UnitCurrentHp -= Damage;
            if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
            {
                ShowInfoPanels();
            }
            return false;
        }
    }

    public virtual void Death()
    {
        if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
        {
            MouseSelectionManager.Instance.ClearSelection();
        }
        CivilizationScriptBehaviour.Instance.AddRemoveCivilizationValue(-UnitStatisticsSO.PopolationsValue);
        UIManager.Instance.RefreshPopulation();
        Destroy(this.gameObject);
    }

    public virtual void ShowInfoPanels()
    {
        m_UnitShowSelectionableInfo.ShowSelectionableInfo(gameObject.transform.name, m_UnitCurrentHp, UnitStatisticsSO.HealthMax, UnitStatisticsSO.Defence, UnitStatisticsSO.UnitSprite,  UnitStatisticsSO.Attack, UnitStatisticsSO.TimeToAttack, UnitStatisticsSO.ViewRadius, UnitStatisticsSO.MovementSpeed, UnitStatisticsSO.UnitcButtons);
    }
}
