﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    
    public GameObject PanelToChooseCivilization;

    [SerializeField] private TextMeshProUGUI m_PopOutText;
    public GameObject PopOutObject;

    [Header("Selected Object info")]
    #region SelectedObject
    
    public GameObject SelectedObjectInfoPanel;
    public GameObject CreateBuildingsPanel;

    [Space, Header("Selected Object Text")]
    [SerializeField] private TextMeshProUGUI[] m_InfosText;
    public TextMeshProUGUI[] InfosText
    {
        get
        {
            return m_InfosText;
        }
        set
        {
            m_InfosText = value;
        }
    }
    public string[] m_InfosTextStartCopy { get; private set; }

    [SerializeField] private Image m_SelectedObjectSprite;

    [Space, Header("Selected Object Buttons")]
    [SerializeField] private GameObject[] m_InfoPanelEmptyButtons = new GameObject[9];
    public GameObject[] InfoPanelEmptyButtons
    {
        get
        {
            return m_InfoPanelEmptyButtons;
        }
        set
        {
            m_InfoPanelEmptyButtons = value;
        }
    }

    [Space, Header("Selected Object Buttons to create buildings")]
    [SerializeField] private GameObject[] m_CreateBuildingsPanelEmptyButtons;
    public GameObject[] CreateBuildingsPanelEmptyButtons
    {
        get
        {
            return m_CreateBuildingsPanelEmptyButtons;
        }
        set
        {
            m_CreateBuildingsPanelEmptyButtons = value;
        }
    }
    #endregion

    #region CivilizationInfo
    [Space, Header("Civilization's resource text")]
    [SerializeField] private TextMeshProUGUI m_StoneText;
    [SerializeField] private TextMeshProUGUI m_GoldText;
    [SerializeField] private TextMeshProUGUI m_WoodText;
    [SerializeField] private TextMeshProUGUI m_FoodText;
    [SerializeField] private TextMeshProUGUI m_PopulationsText;
    #endregion


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        m_InfosTextStartCopy = new string[InfosText.Length];
        m_SelectedObjectSprite = m_SelectedObjectSprite.GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        CopyText();

        ActivateOrDeactivateObjectUI(PanelToChooseCivilization, true);
        DeactivateAllPanels();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public void RefreshResources()
    {
        for (int i = 0; i < CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources.Length; i++)
        {
            if (CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceType == Materials.Stone)
            {
                m_StoneText.text = CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceQuantity.ToString();
            }
            else if (CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceType == Materials.Gold)
            {
                m_GoldText.text = CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceQuantity.ToString();
            }
            else if (CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceType == Materials.Wood)
            {
                m_WoodText.text = CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceQuantity.ToString();
            }
            else if (CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceType == Materials.Food)
            {
                m_FoodText.text = CivilizationScriptBehaviour.Instance.m_CurrentCivilizationResources[i].ResourceQuantity.ToString();
            }
        }
    }

    public void RefreshPopulation()
    {
        m_PopulationsText.text = CivilizationScriptBehaviour.Instance.CivilizationSO.NumberOfPopulation.ToString() + " / " + CivilizationScriptBehaviour.Instance.CurrentCivilizationMaxPopulation.ToString();
    }

    public void InstantiateCivilizationButton()
    {
        int i = 0;

        foreach (KeyValuePair<Buildings, BuildingInfo> Building in CivilizationScriptBehaviour.Instance.CivilizationSO.BuildingsDictionary)
        {
            if(Building.Value.BuildingStatsCopy.BuildingButtonReference != null)
            {
                Button Button = Instantiate(Building.Value.BuildingStatsCopy.BuildingButtonReference, CreateBuildingsPanelEmptyButtons[i].transform.position, Quaternion.identity);
                Button.gameObject.transform.parent = CreateBuildingsPanelEmptyButtons[i].transform;
                Button.gameObject.transform.localScale = new Vector3(0.8f, 1.8f, 1);
                Button.gameObject.name = "Button" + CreateBuildingsPanelEmptyButtons[i].transform.name;
                i++;
            }
        }
    }

    public void DeactivateAllPanels()
    {
        ActivateOrDeactivateObjectUI(CreateBuildingsPanel, false);
        ActivateOrDeactivateObjectUI(SelectedObjectInfoPanel, false);
        ActivateOrDeactivateObjectUI(PopOutObject, false);
    }


    public void ActivateOrDeactivateObjectUI(GameObject Object, bool Activate)
    {
        if(Object != null)
        {
            if (Activate)
            {
                if (Object.active)
                {
                    Object.SetActive(true);
                }
                else
                {
                    Object.SetActive(true);
                }
            }
            else
            {
                if(Object.active)
                {
                    Object.SetActive(false);
                }
                else
                {
                    Object.SetActive(false);
                }
            }  
        }
        else
        {
            Debug.Log("NeedReferenceInIspector");
        }
    }

    public void ChangePopoutText(string Popouttext)
    {
        m_PopOutText.text = Popouttext;
    }

    private void CopyText()
    {
        for (int i = 0; i < InfosText.Length; i++)
        {
            m_InfosTextStartCopy[i] = InfosText[i].text;
        }
    }

    public void ChangeInfoPanelSprite(Sprite NewSprite)
    {
        m_SelectedObjectSprite.sprite = NewSprite;
    }
}
