﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EventHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private string m_TextToShow; 

    #region IPointerEnterHandler implementation

    public void OnPointerEnter(PointerEventData eventData)
    {
        UIManager.Instance.ActivateOrDeactivateObjectUI(UIManager.Instance.PopOutObject, true);
        UIManager.Instance.ChangePopoutText(m_TextToShow);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UIManager.Instance.ActivateOrDeactivateObjectUI(UIManager.Instance.PopOutObject, false);
        UIManager.Instance.ChangePopoutText("");
    }


    #endregion
}
