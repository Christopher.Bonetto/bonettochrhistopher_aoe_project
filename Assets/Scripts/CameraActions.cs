﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActions : MonoBehaviour
{
    [Header("Camera Movement")]

    [SerializeField] private float m_CameraSpeed = 20f;
    [SerializeField] private float m_CameraBorderThickness = 10f;
    [SerializeField] private Vector2 m_CameraLimit;

    [Space, Header("Camera Zoom")]

    [SerializeField] private float m_ScrollSpeed = 2f;
    [SerializeField] private float m_ScrollMinY = 20f;
    [SerializeField] private float m_ScrollMaxY = 120f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = MoveCamera();
    }

    private Vector3 MoveCamera()
    {
        Vector3 CameraPosition = transform.position;

        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - m_CameraBorderThickness)
        {
            CameraPosition.z += m_CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= m_CameraBorderThickness)
        {
            CameraPosition.z -= m_CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - m_CameraBorderThickness)
        {
            CameraPosition.x += m_CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= m_CameraBorderThickness)
        {
            CameraPosition.x -= m_CameraSpeed * Time.deltaTime;
        }

        float Scroll = Input.GetAxis("Mouse ScrollWheel");
        CameraPosition.y -= Scroll * m_ScrollSpeed * Time.deltaTime;

        CameraPosition.x = Mathf.Clamp(CameraPosition.x, -m_CameraLimit.x, m_CameraLimit.x);
        CameraPosition.y = Mathf.Clamp(CameraPosition.y, m_ScrollMinY, m_ScrollMaxY);
        CameraPosition.z = Mathf.Clamp(CameraPosition.z, -m_CameraLimit.y, m_CameraLimit.y);

        return CameraPosition;
    }

}
