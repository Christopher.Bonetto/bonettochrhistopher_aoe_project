﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArrowActions : MonoBehaviour
{
    private Rigidbody m_ArrowRb;
    public int ArrowDamage;
    public GameObject ArrowFocussedObject;

    private void Awake()
    {
        m_ArrowRb = gameObject.GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 4f);
    }
    
    public void AddStartingForce(float Force)
    {
        Debug.Log(Force);
        m_ArrowRb.AddRelativeForce(Vector3.forward * 100 * Force);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.gameObject == ArrowFocussedObject)
        {
            IDamageable CanTakeDamage = other.gameObject.GetComponent<IDamageable>() as IDamageable;
            if (CanTakeDamage != null)
            {
                CanTakeDamage.TakeDamage(ArrowDamage);
                Destroy(this.gameObject);
            }
        }
    }
}
