﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BuildingFoundationsActions : BuildingsScriptBehaviour
{
    public bool CanBeCreated { get; private set; } = true;

    public Buildings m_TypeOfFoundations { get; private set; }

    
    private Renderer m_FoundationsRenderer;
    public Material m_RightPlaceToCreateColor { get; private set; }
    [SerializeField] private Material m_WrongPlaceToCreateColor;
    public Material WrongPlaceToCreate
    {
        get
        {
            return m_WrongPlaceToCreateColor;
        }
    }
    [SerializeField] private Sprite m_FoundationsSprite;

    public override void Awake()
    {
        base.Awake();

        BuildingStatisticsSO = CivilizationScriptBehaviour.Instance.CivilizationSO.BuildingsDictionary[m_TypeOfFoundations].BuildingStatsCopy;
        m_RightPlaceToCreateColor = gameObject.GetComponent<Renderer>().material;
        m_FoundationsRenderer = gameObject.GetComponent<Renderer>();
    }
    public override void Start()
    {
        m_CanBeRepaired = true;
        m_BuildingCurrentHp = 1;
    }
    public override void Update()
    {
        base.Update();
        
    }
    
    public void RotateFoundations()
    {
        transform.Rotate(0,90,0);
    }

    public void SetCanBeCreatedBool(bool NewValue)
    {
        CanBeCreated = NewValue;
    }

    public void ChangeFoundationsMaterial(Material NewMaterial)
    {
        m_FoundationsRenderer.sharedMaterial = NewMaterial;
    }

    public void SetTypeOfFoundations(Buildings NewFoundationsType)
    {
        m_TypeOfFoundations = NewFoundationsType;
    }

    public override void ShowInfoPanels()
    {
        m_BuildingShowSelectionableInfo.ShowSelectionableInfo(gameObject.transform.name, m_BuildingCurrentHp, (int)BuildingStatisticsSO.TimeOfCostruction, 0, m_FoundationsSprite, 0, 0, 0, 0, null);
    }

    public override void RepairOrBuild(int AddHealthValue)
    {
        if(m_CanBeRepaired && m_BuildingCurrentHp < BuildingStatisticsSO.TimeOfCostruction)
        {
            base.RepairOrBuild(AddHealthValue);
        }
        else if (m_BuildingCurrentHp == BuildingStatisticsSO.TimeOfCostruction)
        {
            base.RepairOrBuild(AddHealthValue);
            UIManager.Instance.DeactivateAllPanels();
            CivilizationScriptBehaviour.Instance.InstantiateBuilding(m_TypeOfFoundations, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(this.gameObject);
        }        
    }

    public override bool TakeDamage(int Damage)
    {
        Damage = Mathf.Clamp(Damage, 0, (int)BuildingStatisticsSO.TimeOfCostruction + BuildingStatisticsSO.Defence);
        m_CanBeRepaired = true;
        if (m_BuildingCurrentHp <= Damage)
        {
            m_BuildingCurrentHp -= Damage;

            Death();
            return true;
        }
        else
        {
            m_BuildingCurrentHp -= Damage;
            if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
            {
                ShowInfoPanels();
            }
            return false;
        }
    }

    public override void Death()
    {
        foreach (GameObject Worker in m_WorkersAssignedList)
        {
            Worker.GetComponent<NavMeshAgent>().SetDestination(Worker.transform.position);
            Worker.GetComponent<UnitsScriptBehaviour>().FocusObject = null;
        }
        m_WorkersAssignedList.Clear();
        Destroy(this.gameObject);
    }


    public void OnTriggerEnter(Collider otherObject)
    {
        if (gameObject.GetComponent<Collider>().isTrigger)
        {
            CanBeCreated = false;
            ChangeFoundationsMaterial(m_WrongPlaceToCreateColor);
        }
    }

    public void OnTriggerStay(Collider otherObject)
    {
        if (gameObject.GetComponent<Collider>().isTrigger)
        {
            CanBeCreated = false;
            ChangeFoundationsMaterial(m_WrongPlaceToCreateColor);
        }
    }
    public void OnTriggerExit(Collider otherObject)
    {
        if (gameObject.GetComponent<Collider>().isTrigger)
        {
            CanBeCreated = true;
            ChangeFoundationsMaterial(m_RightPlaceToCreateColor);
        }
    }

    
}
