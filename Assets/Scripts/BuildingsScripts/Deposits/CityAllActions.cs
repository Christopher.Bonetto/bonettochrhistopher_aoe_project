﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CityAllActions : DepositBuildingActions
{
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();        
    }

    // Update is called once per frame
    public override void Update()
    {
        CheckUnitQueue();
        UpgradeToDo();
        RandomNavmeshLocation("Walkable");
    }

    public override void RandomNavmeshLocation(string AreaName)
    {
        if (m_FindSpawnPointPosition)
        {
            NavMeshHit hit;
            int RightMask = 1 << NavMesh.GetAreaFromName(AreaName);

            float radius = gameObject.transform.localScale.x + 4f;
            Vector3 randomDirection = Random.insideUnitSphere * radius;
            randomDirection += transform.position;


            if (NavMesh.SamplePosition(randomDirection, out hit, radius, RightMask))
            {
                if (hit.distance >= gameObject.transform.localScale.x + 1f)
                {
                    SpawnPoint.transform.position = hit.position;
                    ShowHideSpawnPoint();
                    InstantiateUnit(Units.Worker);
                    InstantiateUnit(Units.Worker);
                    InstantiateUnit(Units.ArcherLong);

                    m_FindSpawnPointPosition = false;
                }
            }
        }
    }
}
