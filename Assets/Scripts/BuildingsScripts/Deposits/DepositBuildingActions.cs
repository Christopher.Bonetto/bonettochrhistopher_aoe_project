﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DepositBuildingActions : BuildingsScriptBehaviour
{
    [SerializeField] private Materials[] m_ResourceCanBeAcquired;
    public Materials[] ResourceCanBeAcquired
    {
        get
        {
            return m_ResourceCanBeAcquired;
        }
        set
        {
            m_ResourceCanBeAcquired = value;
        }
    }


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void Death()
    {
        foreach (GameObject Worker in m_WorkersAssignedList)
        {
            if (Worker.GetComponent<UnitsScriptBehaviour>().FocusObject == this.gameObject)
            {
                Worker.GetComponent<NavMeshAgent>().SetDestination(Worker.transform.position);
                Worker.GetComponent<UnitsScriptBehaviour>().FocusObject = null;
            }
        }
        m_WorkersAssignedList.Clear();
        if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
        {
            MouseSelectionManager.Instance.ClearSelection();
        }
        if (CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList.Contains(this.gameObject))
        {
            CivilizationScriptBehaviour.Instance.CivilizationSO.DepositsList.Remove(this.gameObject);
            CivilizationScriptBehaviour.Instance.DepositInstantiated = true;
        }

        Destroy(this.gameObject);
    }
}
