﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseActions : BuildingsScriptBehaviour
{
    public static int IncreaseCivilizationNumber = 5;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        CivilizationScriptBehaviour.Instance.CurrentCivilizationMaxPopulation += IncreaseCivilizationNumber;
        UIManager.Instance.RefreshPopulation();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }
}
