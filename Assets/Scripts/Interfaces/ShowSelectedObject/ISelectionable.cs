﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public interface ISelectionable
{
    void ShowInfoPanels();
}

public interface IShowSelectionableInfo
{
    void ShowSelectionableInfo(string Name = "", int CurrentHp = 0, int MaxHp = 0, int Defence = 0, Sprite ObjectSprite = null, int Attack = 0, float AttackSpeed = 0, int Range = 0, float MovementSpeed = 0, Button[] ComandsButtons = null);
    void ClearInfo();
}
