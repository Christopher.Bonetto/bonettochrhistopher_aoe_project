﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShowSelectedObject : MonoBehaviour, IShowSelectionableInfo
{
    public void ShowSelectionableInfo(string Name = "", int CurrentHp = 0, int MaxHp = 0, int Defence = 0, Sprite ObjectSprite = null, int Attack = 0, float AttackSpeed = 0, int Range = 0, float MovementSpeed = 0, Button[] ComandButtons = null)
    {
        ClearInfo();
        
        float[] Values = new float []{ (float)CurrentHp, (float)MaxHp, (float)Defence, (float)Attack, AttackSpeed, Range, MovementSpeed};
                
        for (int i = 0; i < UIManager.Instance.InfosText.Length; i++)
        {
            if(i == 0)
            {
                UIManager.Instance.InfosText[0].text = UIManager.Instance.InfosText[0].text + Name;
            }
            else if (i == 1)
            {
                UIManager.Instance.InfosText[i].text = UIManager.Instance.InfosText[i].text + CurrentHp.ToString() + " / " + MaxHp.ToString();
            }
            else if (i > 1)
            {
                if (Values[i] == 0)
                {
                    if (UIManager.Instance.InfosText[i].isActiveAndEnabled)
                    {
                        UIManager.Instance.InfosText[i].gameObject.SetActive(false);
                    }
                    else
                    {

                    }
                }
                else
                {
                    if (!UIManager.Instance.InfosText[i].isActiveAndEnabled)
                    {
                        UIManager.Instance.InfosText[i].gameObject.SetActive(true);
                        UIManager.Instance.InfosText[i].text = UIManager.Instance.InfosText[i].text + Values[i].ToString();
                    }
                    else
                    {
                        UIManager.Instance.InfosText[i].text = UIManager.Instance.InfosText[i].text + Values[i].ToString();
                    }
                }

            }
        }

        if (ObjectSprite != null)
        {
            UIManager.Instance.ChangeInfoPanelSprite(ObjectSprite);
        }
        else
        {
            UIManager.Instance.ChangeInfoPanelSprite(null);
        }

        if(ComandButtons == null)
        {
            ChangeInfoPanelButtons();
        }
        else
        {
            ChangeInfoPanelButtons(ComandButtons);
        }

    }

    private void ChangeInfoPanelButtons(Button[] ButtonsToChange = null)
    {
        for (int k = 0; k < UIManager.Instance.InfoPanelEmptyButtons.Length; k++)
        {
            if(UIManager.Instance.InfoPanelEmptyButtons[k].transform.childCount > 0)
            {
                foreach (Transform Child in UIManager.Instance.InfoPanelEmptyButtons[k].transform)
                {
                    Destroy(Child.gameObject);
                }
            }
            if(ButtonsToChange != null)
            {
                if (k < ButtonsToChange.Length)
                {
                    Button Button = Instantiate(ButtonsToChange[k], UIManager.Instance.InfoPanelEmptyButtons[k].transform.position, Quaternion.identity);
                    Button.gameObject.transform.parent = UIManager.Instance.InfoPanelEmptyButtons[k].transform;
                    Button.gameObject.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }

    public void ClearInfo()
    {
        for (int i = 0; i < UIManager.Instance.InfosText.Length; i++)
        {
            UIManager.Instance.InfosText[i].text = UIManager.Instance.m_InfosTextStartCopy[i];
        }
    }
    
}
