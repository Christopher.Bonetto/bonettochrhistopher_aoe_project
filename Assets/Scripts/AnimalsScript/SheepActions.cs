﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepActions : AnimalsActions
{
    [SerializeField] private int m_RadiusToCheckUnit;
    public bool m_Captured { get; private set; } = false;

    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        
        if (!m_Captured)
        {
            CheckNearColliders();
            base.Update();
        }
        else
        {

        }
        
    }

    public void SetSheepDestination(Vector3 DestinationToReach)
    {
        AnimalAgent.SetDestination(DestinationToReach);
    }

    private void CheckNearColliders()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, m_RadiusToCheckUnit, m_LayerToCheck); // must be integrate layer 
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<UnitsScriptBehaviour>())
            {
                AnimalAgent.velocity = Vector3.zero;
                gameObject.layer = hitColliders[0].gameObject.layer;
                m_Captured = true;
            }
        }
    }
}
