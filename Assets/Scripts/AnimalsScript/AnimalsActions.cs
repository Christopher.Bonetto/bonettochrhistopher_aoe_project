﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalsActions : MonoBehaviour, IDamageable, ISelectionable
{
    protected NavMeshAgent m_AnimalAgent;
    public NavMeshAgent AnimalAgent
    {
        get
        {
            return m_AnimalAgent;
        }
        set
        {
            m_AnimalAgent = value;
        }
    }

    [SerializeField] private int m_AnimalMaxHp;
    [SerializeField] private GameObject m_CarcassToSpawn;

    
    [SerializeField] protected LayerMask m_LayerToCheck;
    protected Vector3 m_NextAgentDestination;

    protected List<GameObject> m_UnitAssignedToKillAnimal = new List<GameObject>();

    public IShowSelectionableInfo m_AnimalShowSelectionableInfo { get; protected set; }
    private int m_AnimalCurrentHp = 0;

    [SerializeField] private Sprite m_AnimalSprite;

    public virtual void Awake()
    {
        if (gameObject.GetComponent<NavMeshAgent>() != null)
        {
            AnimalAgent = gameObject.GetComponent<NavMeshAgent>();
        }
        else
        {
            AnimalAgent = gameObject.AddComponent<NavMeshAgent>();
        }
        
        m_AnimalShowSelectionableInfo = new ShowSelectedObject();
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        m_AnimalCurrentHp = m_AnimalMaxHp;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        SetNextAnimalDestination();
      
    }

    public void SetNextAnimalDestination()
    {
        if (!AnimalAgent.hasPath)
        {            
            AnimalAgent.SetDestination(RandomNavmeshLocation(20));
        }
    }
    

    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }


    public void AddUnitToKillList(GameObject Worker)
    {
        if (!m_UnitAssignedToKillAnimal.Contains(Worker))
        {
            m_UnitAssignedToKillAnimal.Add(Worker);
        }
    }

    public bool TakeDamage(int Damage)
    {
        AnimalAgent.ResetPath();
        Damage = Mathf.Clamp(Damage, 0, m_AnimalMaxHp);
        
        if (m_AnimalCurrentHp <= Damage)
        {
            m_AnimalCurrentHp -= Damage;
            UIManager.Instance.DeactivateAllPanels();
            Death();
            return true;
        }
        else
        {
            m_AnimalCurrentHp -= Damage;
            if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
            {
                ShowInfoPanels();
            }
            return false;
        }
        
    }

    public void Death()
    {
        foreach (GameObject Worker in m_UnitAssignedToKillAnimal)
        {
            if (Worker.GetComponent<UnitsScriptBehaviour>().FocusObject == this.gameObject)
            {
                Worker.GetComponent<NavMeshAgent>().SetDestination(Worker.transform.position);
                Worker.GetComponent<UnitsScriptBehaviour>().FocusObject = null;
            }
        }
        if (m_CarcassToSpawn != null)
        {
            GameObject Carcass = Instantiate(m_CarcassToSpawn, gameObject.transform.position, Quaternion.identity);
            Carcass.transform.name = m_CarcassToSpawn.transform.name;
        }
        if (gameObject == MouseSelectionManager.Instance.CurrentSelectedObject)
        {
            MouseSelectionManager.Instance.ClearSelection();
        }
        Destroy(this.gameObject);
    }

    public void ShowInfoPanels()
    {
        m_AnimalShowSelectionableInfo.ShowSelectionableInfo(gameObject.name, m_AnimalCurrentHp, m_AnimalMaxHp, 0, m_AnimalSprite, 0, 0, 0, 0, null);
    }
}
